<?php

namespace App\Http\Controllers;

use App\Models\WeatherParam;
use http\Message;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Http;

class WeatherController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $weatherParams = WeatherParam::all();

        return view('main', compact('weatherParams'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $weatherParam = $request->data;

            $savedItems = WeatherParam::create([
                'name' => $weatherParam['cityName'],
                'country' => $weatherParam['country'],
                'temp_c' => $weatherParam['temperature'],
                'feels_like_c' => $weatherParam['feelsLike'],
                'humidity' => $weatherParam['humidity'],
                'condition_text' => $weatherParam['text'],
                'icon' => $weatherParam['icon']
            ]);

            return WeatherParam::all();
    }

    public function searchWeatherByCityName(Request $request)
    {
        $cityName = $request->searchWeatherByCityName;
        $apiKey = 'bc3a4d44075647ecade173718211902';

        if($cityName){
            try {
                $getWeatherByCityNameRequest = Http::get("http://api.weatherapi.com/v1/current.json?key=$apiKey&q=$cityName&aqi=no");
            }
            catch(\Exception $e) {
                return redirect()->back()->withErrors(['There was an error while trying to find the weather, please try again in 5 minutes.']);
            }

            $getWeatherByCityNameResponse = $getWeatherByCityNameRequest->json();

            if(empty($getWeatherByCityNameResponse)){
                return redirect()->back()->withErrors(['Weather was not found for the selected country.']);
            }

            $weatherParamsForLocation = $getWeatherByCityNameResponse['location'];
            $name = $weatherParamsForLocation['name'];
            $country = $weatherParamsForLocation['country'];

            $weatherParamsForCurrent = $getWeatherByCityNameResponse['current'];
            $temperature = $weatherParamsForCurrent['temp_c'];
            $feelsLike = $weatherParamsForCurrent['feelslike_c'];
            $humidity = $weatherParamsForCurrent['humidity'];

            $weatherParamsForCurrentCondition = $weatherParamsForCurrent['condition'];
            $conditionText = $weatherParamsForCurrentCondition['text'];
            $conditionIcon = $weatherParamsForCurrentCondition['icon'];

            return response()->json([
                'success' => true,
                'name' => $name,
                'country' => $country,
                'temp_c' => $temperature,
                'feels_like_c' => $feelsLike,
                'humidity' => $humidity,
                'condition_text' => $conditionText,
                'icon' => $conditionIcon
            ], 200);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function deleteAddedCity($id)
    {
        $weatherParam = WeatherParam::find($id);
        $weatherParam->delete();

        return WeatherParam::all();
    }

    public function getWeatherData(){
        $weatherParams = WeatherParam::all();

        return $weatherParams;
    }
}
