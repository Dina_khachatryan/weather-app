<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class WeatherParam extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'country',
        'temp_c',
        'feels_like_c',
        'humidity',
        'condition_text',
        'icon'
    ];
}
