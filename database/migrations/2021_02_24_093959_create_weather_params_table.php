<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWeatherParamsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('weather_params', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('country');
            $table->string('temp_c');
            $table->string('feels_like_c');
            $table->string('humidity');
            $table->string('condition_text');
            $table->string('icon');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('weather_params');
    }
}
