<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Weather App</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">


        <!-- Styles -->
        <style>
            body {
                font-family: "Times New Roman";
                background: url("https://images.pexels.com/photos/691668/pexels-photo-691668.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260") no-repeat center center fixed;

                -webkit-background-size: cover;
                -moz-background-size: cover;
                -o-background-size: cover;
                background-size: cover;

            }
            .weather-card {
                margin: 60px auto;
                height: 445px;
                width: 300px;
                background: #fff;
                box-shadow: 0 1px 38px rgba(0, 0, 0, 0.15), 0 5px 12px rgba(0, 0, 0, 0.25);
                overflow: hidden;
                border-radius: 6px;
            }
            .weather-card .top {
                position: relative;
                height: 380px;
                width: 100%;
                overflow: hidden;
                background-color: white;
                background-size: cover;
                background-position: center center;
                text-align: center;
            }
            .weather-card .top .wrapper {
                padding: 10px;
                position: relative;
                z-index: 1;
            }

            .weather-card .top .wrapper .heading {
                margin-top: 20px;
                font-size: 35px;
                font-weight: 400;
                color: #fff;
            }
            .weather-card .top .wrapper .location {
                margin-top: 20px;
                font-size: 21px;
                font-weight: 400;
                color: #fff;
            }
            .weather-card .top .wrapper .temp {
                margin-top: 0px;
                margin-bottom: 0px;
            }
            .weather-card .top .wrapper .temp a {
                text-decoration: none;
                color: #fff;
            }
            .weather-card .top .wrapper .temp .temp-type {
                font-size: 85px;
                color: white;
            }
            .weather-card .top .wrapper .temp .temp-value {
                display: inline-block;
                font-size: 85px;
                font-weight: 600;
                color: #fff;
            }
            .weather-card .top .wrapper .temp .deg {
                display: inline-block;
                font-size: 35px;
                font-weight: 600;
                color: #fff;
                vertical-align: top;
                margin-top: 10px;
            }
            .weather-card .top:after {
                content: "";
                height: 100%;
                width: 100%;
                display: block;
                position: absolute;
                top: 0;
                left: 0;
                background: rgba(0, 0, 0, 0.6);
            }
            .weather-card .bottom {

                padding: 0 30px;
                background: #fff;
            }
            .weather-card .bottom .wrapper .forecast {
                overflow: hidden;
                margin: 0;
                font-size: 0;
                padding: 0;
                padding-top: 20px;
                max-height: 155px;
            }
            h1{
                text-align: center;
                margin-top: 50px;
                font-size: 60px;
                font-weight: 700;
                color: white;
            }

            .search-weather-by-city{
                margin-bottom: 30px;
            }

            .search-weather-by-city .input-group_item-search {
                display: flex;
                justify-content: space-between;
            }
            .search-weather-by-city .input-group_item-search .input-group-prepend{
                margin-left: 10px !important;
            }

            .weather-card .add-city{
                border: none;
                background-color: white;
                display: flex;
            }

            .city-list-group {
                background: none;
                display: inline-block;
            }
            .city-list-group .city-list {
                background-color: white;
                margin-right: 20px;
            }
            .city-list-group .city-list .delete-btn{
                background-color: white;
                border:none;
            }

            .weather-forecast {
                width: 700px;
                height: 480px;
                text-align: center;
            }
            .weather-forecast .card-title{
                margin-top: 20px;
                font-weight: 600;
                font-size: 40px;
            }
            .weather-forecast .day-list {
                list-style-type: none;
            }
            .weather-forecast .day-list .day-list-value {
                display: flex;
                justify-content: space-between;
                align-items: center;
            }
            .weather-forecast .day-list .day-list-value .parameters{
                width: 60%;
                display: flex;
                justify-content: space-around;
                align-items: center;
            }
        </style>

    </head>

    <body>
        <div id="app">
            <show-weather></show-weather>
        </div>
    </body>

</html>

<!-- Scripts -->
<script type="text/javascript" src="../js/app.js"></script>
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="https://unpkg.com/axios/dist/axios.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/js/bootstrap.bundle.min.js" integrity="sha384-b5kHyXgcpbZJO/tY9Ul7kGkf1S0CWuKcCD38l8YkeH8z8QjE0GmW1gYU5S9FOnJ0" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
