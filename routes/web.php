<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\WeatherController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\WeatherController::class, 'index']);
Route::post('/search-weather', [App\Http\Controllers\WeatherController::class, 'searchWeatherByCityName']);
Route::post('/add-weather', [App\Http\Controllers\WeatherController::class, 'store']);
Route::get('/get-weather', [App\Http\Controllers\WeatherController::class, 'getWeatherData']);
Route::get('/delete-city/{id}', [App\Http\Controllers\WeatherController::class, 'deleteAddedCity']);


